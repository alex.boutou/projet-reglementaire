from pandas import DataFrame, concat
from lxml import etree
from inies import get_fdes_attrib
import ast
import dataset
from os import path
from eval import changed_unit

db = dataset.connect('sqlite:///BDD_INIES.db')


def final_eges(init_df,id_to_change,new_id,session_id,DVT,sdp,tacconso,taccoge):
    new_df=modif_df(init_df,id_to_change,new_id,session_id,DVT,tacconso,taccoge)
    return return_eges_values(new_df,sdp)

def return_eges_values(new_df,sdp_list):
    eges=[]
    egespce=[]
    i=0
    for df in new_df:
        sdp=sdp_list[i]
        eges_bat=df['contribution'].sum()
        egespce_bat=df.loc[df.tag=='donnees_composant']['contribution'].sum()
        egespce_bat+=df.loc[df.tag=='autoproduction']['contribution'].sum()
        egespce_bat+=df.loc[df.tag=='cogeneration']['contribution'].sum()
        i+=1
        eges.append(eges_bat/sdp)
        egespce.append(egespce_bat/sdp)
    return eges, egespce

def modif_df(init_df,id_to_change,new_id,session_id,DVT,tacconso_list,taccoge_list):
    i=0
    returned=[]
    for df in init_df:
        tacconso=tacconso_list[i]
        taccoge=taccoge_list[i]
        new_df=df.copy()
        table=db['Indicateurs']
        if (not changed_unit(id_to_change) and not changed_unit(new_id)):
            #print('UNITE IDENTIQUE')
            data=new_df.loc[new_df['id_inies']==id_to_change].copy()
            new_df=new_df[new_df.id_inies != id_to_change]
            fdes=get_fdes_attrib(new_id,session_id)
            norme=fdes['Indicateur_Set']['Norme_ID']
            data['id_inies']=new_id
            dve=fdes['DVT']
            if dve==0:
                renew=1
            else:
                renew=max(1,DVT/dve)
            data['renew']=renew
            data['eg_part']=to_add(fdes,norme)
        else:
            #print('CHANGEMENT UNITE')
            data=new_df.loc[new_df['id_inies']==id_to_change].copy()
            new_df=new_df[new_df.id_inies != id_to_change]
            fdes=get_fdes_attrib(new_id,session_id)
            norme=fdes['Indicateur_Set']['Norme_ID']
            data['id_inies']=new_id
            dve=fdes['DVT']
            if dve==0:
                renew=1
            else:
                renew=max(1,DVT/dve)
            data['renew']=renew
            data['eg_part']=to_add(fdes,norme)
            try :
                coef_old_id=ast.literal_eval(table.find_one(id_inies=id_to_change)['Autre coefficient'])
            except:
                coef_old_id=1
            try:
                coef_new_id=ast.literal_eval(table.find_one(id_inies=new_id)['Autre coefficient'])
            except:
                coef_new_id=1
            data['quantite']=data['quantite']*coef_old_id/coef_new_id
        data['contribution']=data['quantite']*data['renew']*data['eg_part']
        if data['tag'].tolist()==['autoproduction']:
            data['contribution']*=tacconso
        if data['tag'].tolist()==['cogeneration']:
            data['contribution']*=taccoge
        returned.append(concat([new_df,data]))
        i+=1
    return returned


def build_df(RSEE,session_id,DVT):
    root=RSEE.xpath('.//entree_projet')[0]
    bat_frame=[]
    for child in root:
        if child.tag=="batiment":
            tacconso=ast.literal_eval(root.xpath('.//autoconsommation_pv')[0].text)/100
            taccoge=ast.literal_eval(root.xpath('.//autoconsommation_cogeneration')[0].text)/100
            df_temp=eges_recursif(child,session_id,DVT)
            if tacconso>0:
                df_temp.loc[df_temp.tag == 'autoproduction','contribution']= df_temp.loc[df_temp.tag == 'autoproduction','contribution']*tacconso
            if taccoge>0:
                df_temp.loc[df_temp.tag == 'cogeneration','contribution']= df_temp.loc[df_temp.tag == 'autoproduction','contribution']*taccoge
            df_temp['id_inies'] = df_temp.index
            df_temp.groupby(['id_inies','tag','renew','eg_part'],as_index=False,sort=False,group_keys=False).sum()
            bat_frame.append(df_temp)
    return bat_frame

def eges_recursif(tree,session_id,DVT):
    if len(tree)==0:
        return DataFrame()
    if (tree.find('donnees_service')!=None):
        frames=[]
        for fiche in tree:
            frames.append(calc_eges_df(fiche,DVT,session_id,'donnees_service'))
        return concat(frames)
    if (tree.find('donnees_composant')!=None):
        tag=tree.tag
        ref=tree.get('ref')
        frames=[]
        if tag=="lot" and ref=="13":
            for fiche in tree:
                frames.append(calc_eges_df(fiche,DVT,session_id,'autoproduction'))
            return concat(frames)
        if tag=="sous_lot" and ref=="2":
            parent=tree.getparent()
            parent_tag=parent.tag
            parent_ref=parent.get('ref')
            if parent_tag=="lot" and parent_ref=="8":
                for fiche in tree:
                    frames.append(calc_eges_df(fiche,DVT,session_id,'cogeneration'))
                return concat(frames)
            else:
                for fiche in tree:
                    frames.append(calc_eges_df(fiche,DVT,session_id,'donnees_composant'))
                return concat(frames)
        else:
            for fiche in tree:
                frames.append(calc_eges_df(fiche,DVT,session_id,'donnees_composant'))
            return concat(frames)
    else:
        frames=[]
        for child in tree:
            frames.append(eges_recursif(child,session_id,DVT))
        return concat(frames)

def calc_eges_df(element,DVT,session_id,tag):
    id_fiche=ast.literal_eval(element.find('id_fiche').text)
    quantite=ast.literal_eval(element.find('quantite').text)
    try:
        dve=ast.literal_eval(element.find('dve').text)
    except:
        dve=0
    fdes=get_fdes_attrib(id_fiche,session_id)
    if dve==0:
        renew=1
    else:
        renew=max(1,DVT/dve)
    norme=fdes['Indicateur_Set']['Norme_ID']
    eg_part=to_add(fdes,norme)
    contribution=eg_part*renew*quantite
    return DataFrame({'tag': [tag],'quantite': [quantite], 'renew': [renew], 'eg_part':[eg_part], 'contribution':[contribution]}, index = [id_fiche]) 
                
def to_add(fdes,norme):                                                      
    if norme==1:
        return to_add_norme1(fdes)
    if norme==2:
        return to_add_norme2(fdes)
    if norme==3:
        return to_add_norme3(fdes)
    if norme==4:
        return to_add_norme4(fdes)

def to_add_norme1(fdes):
    eg_part=0
    for indic in fdes['Indicateur_Set']['List_Indicateur_Quantities']['Indicateur_Quantity']: 
        if indic['Indicateur_ID']==2 and indic['Phase_ID']==7:
            eg_part+=indic['Quantity']
        if indic['Indicateur_ID']==2 and indic['Phase_ID']==27:
            eg_part-=indic['Quantity']/3
        if indic['Indicateur_ID']==2 and indic['Phase_ID']==4:
            if fdes['isPEP']:
                eg_part-=indic['Quantity']

    return eg_part

def to_add_norme2(fdes):
    eg_part=0
    for indic in fdes['Indicateur_Set']['List_Indicateur_Quantities']['Indicateur_Quantity']: 
        if indic['Indicateur_ID']==19 and indic['Phase_ID']==7:
            eg_part+=indic['Quantity']
        if indic['Indicateur_ID']==19 and indic['Phase_ID']==27:
            eg_part-=indic['Quantity']/3
        if indic['Indicateur_ID']==19 and indic['Phase_ID']==29:
            if fdes['isPEP']:
                eg_part-=indic['Quantity']
    return eg_part

def to_add_norme3(fdes):
    eg_part=0
    for indic in fdes['Indicateur_Set']['List_Indicateur_Quantities']['Indicateur_Quantity']: 
        if indic['Indicateur_ID']==45 and indic['Phase_ID']==7:
            eg_part+=indic['Quantity']/1000
        if indic['Indicateur_ID']==45 and indic['Phase_ID']==4:
            if fdes['isPEP']:
                eg_part-=indic['Quantity']/1000
    return eg_part

def to_add_norme4(fdes):
    eg_part=0
    for indic in fdes['Indicateur_Set']['List_Indicateur_Quantities']['Indicateur_Quantity']: 
        if indic['Indicateur_ID']==19 and indic['Phase_ID']==7:
            eg_part+=indic['Quantity']
        if indic['Indicateur_ID']==19 and indic['Phase_ID']==4:
            if fdes['isPEP']:
                eg_part-=indic['Quantity']
    return eg_part