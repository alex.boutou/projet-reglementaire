from lxml import etree
import ast
import dataset
import copy

from inies import get_fdes_attrib
from os import path

def eges(RSEE,session_id,DVT):
    root=RSEE.xpath('.//entree_projet')[0]
    for child in root:
        if child.tag=="batiment":
            sdp=ast.literal_eval(root.xpath('.//sdp')[0].text)
            tac=ast.literal_eval(root.xpath('.//autoconsommation_pv')[0].text)/100
            eges,eges_pce=eges_recursif(child,session_id,DVT,tac)
            print('EGES')
            print(eges/sdp)
            print('EGESPCE')
            print(eges_pce/sdp)

    return eges/sdp,eges_pce/sdp

def eges_recursif(tree,session_id,DVT,tac):
    if len(tree)==0:
        return (0,0)
    if (tree.find('donnees_service')!=None):
        S=0
        tag=tree.tag
        ref=tree.get('ref')
        for fiche in tree:
            S+=calc_eges_part(fiche,DVT,session_id)
        return S,0

    if (tree.find('donnees_composant')!=None):
        S=0
        tag=tree.tag
        ref=tree.get('ref')
        if tag=="lot" and ref=="13":
            for fiche in tree:
                S+=calc_eges_part(fiche,DVT,session_id)*tac
            return S,S
        else:
            for fiche in tree:
                S+=calc_eges_part(fiche,DVT,session_id)
            return S,S
    else:
        eges_val=0
        egespce_val=0
        for child in tree:
            eges_val_contrib,egespce_val_contrib=eges_recursif(child,session_id,DVT,tac)
            eges_val+=eges_val_contrib
            egespce_val+=egespce_val_contrib
        return eges_val,egespce_val




def calc_eges_part(element,DVT,session_id):
    id_fiche=ast.literal_eval(element.find('id_fiche').text) 
    quantite=ast.literal_eval(element.find('quantite').text)
    try:
        dve=ast.literal_eval(element.find('dve').text)
    except:
        dve=0
    fdes=get_fdes_attrib(id_fiche,session_id)
    if dve==0:
        renew=1
    else:
        renew=max(1,DVT/dve)
    norme=fdes['Indicateur_Set']['Norme_ID']
    eg_part=to_add(fdes,norme)*quantite 
    if (fdes['UF_Unit']=="kWh") or (fdes['UF_Unit']=="m³"):
        eg_part*=1
    else:
        eg_part*=renew
    return eg_part
                
def to_add(fdes,norme):                                                      
    if norme==1:
        return to_add_norme1(fdes)
    if norme==2:
        return to_add_norme2(fdes)
    if norme==3:
        return to_add_norme3(fdes)
    if norme==4:
        return to_add_norme4(fdes)

def to_add_norme1(fdes):
    eg_part=0
    for indic in fdes['Indicateur_Set']['List_Indicateur_Quantities']['Indicateur_Quantity']: 
        if indic['Indicateur_ID']==2 and indic['Phase_ID']==7:
            eg_part+=indic['Quantity']
        if indic['Indicateur_ID']==2 and indic['Phase_ID']==27:
            eg_part+=indic['Quantity']/3
        if indic['Indicateur_ID']==2 and indic['Phase_ID']==4:
            if fdes['isPEP']:
                eg_part-=indic['Quantity']

    return eg_part

def to_add_norme2(fdes):
    eg_part=0
    for indic in fdes['Indicateur_Set']['List_Indicateur_Quantities']['Indicateur_Quantity']: 
        if indic['Indicateur_ID']==19 and indic['Phase_ID']==7:
            eg_part+=indic['Quantity']
        if indic['Indicateur_ID']==19 and indic['Phase_ID']==27:
            eg_part+=indic['Quantity']/3
        if indic['Indicateur_ID']==19 and indic['Phase_ID']==29:
            if fdes['isPEP']:
                eg_part-=indic['Quantity']
    return eg_part

def to_add_norme3(fdes):
    eg_part=0
    for indic in fdes['Indicateur_Set']['List_Indicateur_Quantities']['Indicateur_Quantity']: 
        if indic['Indicateur_ID']==45 and indic['Phase_ID']==7:
            eg_part+=indic['Quantity']/1000
        if indic['Indicateur_ID']==45 and indic['Phase_ID']==4:
            if fdes['isPEP']:
                eg_part-=indic['Quantity']/1000

    return eg_part

def to_add_norme4(fdes):
    eg_part=0
    for indic in fdes['Indicateur_Set']['List_Indicateur_Quantities']['Indicateur_Quantity']: 
        if indic['Indicateur_ID']==19 and indic['Phase_ID']==7:
            eg_part+=indic['Quantity']
        if indic['Indicateur_ID']==19 and indic['Phase_ID']==4:
            if fdes['isPEP']:
                eg_part-=indic['Quantity']
    return eg_part

