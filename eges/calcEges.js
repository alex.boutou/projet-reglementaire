import { getIndics } from "./calcIndicateur.js";

export { egesGlobalMax, egesPCEMax, getEgesS, getEges, getEgesPCE, extractEges };

var residentiel = [1, 2, 6, 8, 10, 11, 12, 13, 27, 30];

function extractEges(batimentSort) {
    for (var value of batimentSort) {
        if(value.key = 'Impacts environnementaux'){
            let listIndic = value.values;
            for (var indic of listIndic) {
                if (indic.key == "Potentiel de réchauffement climatique") {
                    let eges = 0;
                    for (var phase of indic["values"]) {
                        if (phase.key == "8") eges += phase["value"]["sum"];
                        if ((phase.key == "5") && phase["value"]["sum"] > 0) {
                            eges -= phase["value"]["sum"];
                        }
                    }
                    return eges
                }
            }
        }
    }
} 

function getEges(batData, dvt) {
    if(!batData.data){
        return null;
    }
    let eges = extractEges(getIndics(batData, dvt));
    return eges / batData["data"]["sdpZone"];
}

function getEgesPCE(batData, dvt) {
    if(!batData.data){
        return null;
    }
    let filter = "Contributeur PCE";
    let eges = extractEges(getIndics(batData, dvt, filter));
    return eges / batData["data"]["sdpZone"];
}

function egesGlobalMax(batData, dep, alt, ce) {
    var usg = parseInt(batData["usage"] || 0);
    var nbl = parseInt(batData["nbUnitFonc"]);
    var srt = batData["srt"];
    var alt = alt || rset["Sortie_Projet"]["Altitude"];
    var dep = dep || rset["Sortie_Projet"]["Departement"];

    var eges1 = pivotGlobal(usg)[0] + mC1(usg, dep, alt, nbl, srt, ce) + mPark(batData)
    var eges2 = pivotGlobal(usg)[1] + mC2(usg, dep, alt, nbl, srt, ce) + mPark(batData)

    return [eges1, eges2]
}

function egesPCEMax(batData, dep, alt) {
    var nUsage = parseInt(batData["usage"] || 0);
    var eges1 = pivotPCE(nUsage)[0] + mPark(batData)
    var eges2 = pivotPCE(nUsage)[1] + mPark(batData)

    return [eges1, eges2]
}

function mPark(batData) {

    let parkSurf = batData["nbParkSurf"] || 0;
    let parkSouter = batData["nbParkSouter"] || 0;
    let sdpZone = batData["sdpZone"];

    return (parkSurf * 700 + parkSouter * 3000) / sdpZone;
}

function mC1(nUsage, dep, alt, nbl, srt, ce) {
    var m1 = mGeo(dep, nUsage, ce) + mAlt(alt, nUsage);
    m1 += mSurf(nUsage, srt, nbl, ce);
    m1 *= mType(nUsage, ce);
    m1 = mAlpha(nUsage)[0] * (m1 - 1);

    return m1
}

function mC2(nUsage, dep, alt, nbl, srt, ce) {
    var m2 = mGeo(dep, nUsage, ce) + mAlt(alt, nUsage);
    m2 += mSurf(nUsage, srt, nbl, ce);
    m2 *= mType(nUsage, ce);
    m2 = mAlpha(nUsage)[1] * (m2 - 1);

    return m2
}

function pivotGlobal(nUsage) {
    if (nUsage == 1) {
        return [1350, 800];
    } else if (nUsage == 2) {
        return [1550, 1000];
    } else if (nUsage == 16) {
        return [1500, 980];
    } else {
        return [1625, 850];
    }
}

function pivotPCE(nUsage) {
    if (nUsage == 1) {
        return [700, 650];
    } else if (nUsage == 2) {
        return [800, 750];
    } else if (nUsage == 16) {
        return [1050, 900];
    } else {
        return [1050, 750];
    }
}

function mType(nUsage, ce) {
    nUsage = nUsage || 0;
    let values = {
        "0": [1.0, 1.0], // Autres usages
        "1": [1.0, 1.1], // Maison individuelle
        "2": [1.0, 1.1], // Logement collectif
        "3": [1.7, 1.8], // Etablissement accueil petite enfance
        "4": [2.0, 2.3], // Enseignement primaire
        "5": [1.1, 1.2], // Enseignement secondaire (partie jour)
        "6": [2.1, 2.2], // Enseignement secondaire (partie nuit)
        "7": [1.1, 1.3], // Enseignement Université
        "8": [1.4, 1.5], // Foyer de jeunes travailleurs
        "10": [1.4, 1.5], // Hôtel 1* (partie nuit)
        "11": [1.8, 1.9], // Hôtel 2* (partie nuit)
        "12": [1.5, 1.7], // Hôtel 3* (partie nuit)
        "13": [1.5, 1.6], // Hôtel 4* (partie nuit)
        "14": [2.3, 2.4], // Hôtel 1* et 2* (partie jour)
        "15": [2.1, 2.2], // Hôtel 3* et 4* (partie jour)
        "16": [1.0, 1.2], // Bureaux
        "17": [2.8, 3.1], // Restauration commerciale en continue
        "18": [1.5, 1.6], // Restauration 1 repas par jour 5 jours sur 7
        "19": [2.7, 3.0], // Restauration 2 repas par jour 7 jours sur 7
        "20": [2.5, 2.6], // Restauration 2 repas par jour 6 jours sur 7
        "22": [3.3, 4.1], // Commerce, magasin, zones commerciales
        "24": [1.2, 1.3], // Etablissement sportif scolaire
        "26": [1.4, 1.5], // Etablissement sanitaire avec hébergement
        "27": [2.9, 3.0], // Hôpital partie nuit
        "28": [2.0, 2.1], // Hôpital partie jour
        "29": [2.2, 2.5], // Transport - aérogare
        "30": [1.4, 1.5], // Cité universitaire
        "32": [6.8, 6.9], // Industrie fonctionnement en 3x8h, 7 jours sur 7
        "33": [2.7, 2.8], // Industrie 8h-18h
        "34": [1.0, 1.2], // Tribunal
        "36": [2.6, 2.7], // Etablissement sportif municipal ou privé
        "37": [1.6, 1.8], // Restauration Scolaire 1 repas par jour 5 jours sur 7
        "38": [2.4, 2.7], // Restauration Scolaire 3 repas par jour 5 jours sur 7
    }
    return values[nUsage][parseInt(ce) - 1]
}

function mGeo(dep, nUsage, ce) {
    var H1a = ["2", "14", "27", "28", "59", "60", "61", "62", "75", "76", "77"];
    H1a.push(...["78", "80", "91", "92", "93", "94", "95"]);

    var H1b = ["8", "10", "45", "51", "52", "54", "55", "57", "58", "67", "68"];
    H1b.push(...["70", "88", "89", "90"]);

    var H1c = ["1", "3", "5", "15", "19", "21", "23", "25", "38", "39", "42"];
    H1c.push(...["43", "63", "69", "71", "73", "74", "84"]);

    var H2a = ["22", "29", "35", "50", "56"];

    var H2b = ["16", "17", "18", "36", "37", "41", "44", "49", "53", "72"];
    H2b.push(...["79", "85", "86"]);

    var H2c = ["7","9", "12", "24", "31", "32", "33", "40", "46", "47", "65"] ;
    H2c.push(...["81", "82"]);

    var H2d = ["4", "26", "48", "84"];

    var H3 = ["2A", "2B", "6", "11", "13", "30", "34", "66", "83"];

    if (H1a.indexOf(dep) >= 0) {
        return residentiel.indexOf(nUsage) >= 0 ? 1.2 : [1.1, 1.0][parseInt(ce) - 1];
    } else if (H1b.indexOf(dep) >= 0) {
        return residentiel.indexOf(nUsage) >= 0 ? 1.3 : [1.2, 1.0][parseInt(ce) - 1];
    } else if (H1c.indexOf(dep) >= 0) {
        return residentiel.indexOf(nUsage) >= 0 ? 1.2 : [1.1, 1.0][parseInt(ce) - 1];
    } else if (H2a.indexOf(dep) >= 0) {
        return residentiel.indexOf(nUsage) >= 0 ? 1.1 : [1.1, 1.0][parseInt(ce) - 1];
    } else if (H2b.indexOf(dep) >= 0) {
        return residentiel.indexOf(nUsage) >= 0 ? 1.0 : [1.0, 1.0][parseInt(ce) - 1];
    } else if (H2c.indexOf(dep) >= 0) {
        return residentiel.indexOf(nUsage) >= 0 ? 0.9 : [0.9, 1.0][parseInt(ce) - 1];
    } else if (H2d.indexOf(dep) >= 0) {
        return residentiel.indexOf(nUsage) >= 0 ? 0.9 : [0.9, 1.0][parseInt(ce) - 1];
    } else if (H3.indexOf(dep) >= 0) {
        return residentiel.indexOf(nUsage) >= 0 ? 0.8 : [0.8, 1.0][parseInt(ce) - 1];
    }
}

function mAlt(alt, nUsage) {
    if (alt <= 400) {
        return residentiel.indexOf(nUsage) >= 0 ? 0.0 : 0.0;
    } else if (alt <= 800) {
        return residentiel.indexOf(nUsage) >= 0 ?  0.2 : 0.1;
    } else {
        return residentiel.indexOf(nUsage) >= 0 ? 0.4 : 0.2;
    }
}

function mAlpha(nUsage) {
    if (nUsage == 1) {
        return [550, 100];
    } else if (nUsage == 2) {
        return [600, 250];
    } else if (nUsage == 16) {
        return [300, 130];
    } else {
        return [525, 100];
    }
}

function mSurf(nUsage, srt, nbl, ce) {
    if (nUsage == "1") {
        let a = srt / nbl;
        let type = mType(nUsage, ce);

        if (a <= 120) return (0.6 - 0.005 * a) / type;
        if (120 < a && a <= 140) return 0;
        if (140 < a && a <= 200) return (7 / 15 - a / 300) / type;
        if (200 < a) return (-0.2) / type;
    } else if (nUsage == "2") {
        let a = srt / nbl;
        let type = mType(nUsage, ce);

        if (a < 40) return (31 / 25 - a / 40) / type;
        if (40 <= a && a <= 80) return (70 - 3 * a) / 500 / (type - 1);
        if (80 < a && a <= 100) return 0;
        if (100 < a && a <= 150) return (350 - 3 * a) / 250 / (type - 1);
        if (150 < a) return (-0.2) / type;
    } else if (nUsage == "22") {
        let a = srt;
        if (ce == 1) {
            if (a <= 1000) return (a - 1000) / 6270;
            if (a > 1000) return 0;
        } else {
            if (a <= 500) return (500 - a) / 2400;
            if (a > 500) return 0;
        }
    } else if (nUsage == "24" || nUsage == "36") {
        let a = srt;
        if (a <= 1000) return (-0.011 * a) + 1.4;
        if (1000 < a && a <= 2000) return (-0.0003 * a) + 0.6;
        if (a > 2000) return 0;
    } else {
        return 0;
    }
}

function getEgesS(model){
    if(model.name == 'Projet'){
        return getSFromProjet(model);
    }
    else{
        return getSFromBatiment(model);
    }
}

function getSFromProjet(model){
    model.children.map( (batiment) => {
        let ce1 = 0;
        let ce2 = 0;

        let categC1C2 = batiment.children.map( (zone) => {
            if (zone.data.categ == 1) ce1 += parseFloat(zone.data.srt);
            if (zone.data.categ == 2) ce2 += parseFloat(zone.data.srt);
        })
    })

    return ce1 > ce2 ? 1 : 2;
}

function getSFromBatiment(model){
    let ce1 = 0;
    let ce2 = 0;

    let categC1C2 = model.children.map( (zone) => {
        if (zone.data.categ == 1) ce1 += parseFloat(zone.data.srt);
        if (zone.data.categ == 2) ce2 += parseFloat(zone.data.srt);
    })

    return ce1 > ce2 ? 1 : 2;
}
