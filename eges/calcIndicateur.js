import * as d3 from "d3";
import { setModuleD } from "./calcModuleD.js";
import { normeName } from "./calcNameNorme.js";

export { sumFdes, getColumns, getIndics, filtIndics, filtIndicsEges, getIndicsExport, getIndicsExportContrib };

//Get one indicateur for one phase from the indicateurs list
function filtIndics(listIndic) {
    for (var indic of listIndic) {
        if (indic.key == "Potentiel de réchauffement climatique") {
            for (var phase of indic["values"]) {
                if (phase.key == "8") {
                    return phase["value"]["sum"]
                }
            }
        }
    }
}

//Get one indicateur for one phase from the extended table indicateurs list
function filtIndicsEges(batimentSort) {
    for (var value of batimentSort){
        if(value.key = 'Impacts environnementaux'){
            let listIndic = value.values;
            for (var indic of listIndic) {
                if (indic.key == "Potentiel de réchauffement climatique") {
                    for (var phase of indic["values"]) {
                        if (phase.key == "8") {
                            return phase["value"]["sum"]
                        }
                    }
                }
            }
        }
    }

    return 0;
}

//Get clean list of indicateurs
function getIndics(model, dvt, filter="") {
    let newModel = setModuleD(model);
    let list = sumIndic(normeName(extractIndics(newModel, dvt, filter)));
    return list;
}

function getIndicsExportContrib(model, dvt, filter="") {
    let newModel = setModuleD(model);
    return sumExport(normeName(extractIndics(newModel, dvt, filter)));
}

function getIndicsExport(model, dvt, level, filter="") {
    if(model.name === level){
        let newModel = setModuleD(model);
        return sumExport(normeName(extractIndics(newModel, dvt, filter)));
    }
    else{
        let returnArray = [];
        model.children.map( (child) => {
            returnArray.push(getIndicsExport(child,dvt,level,filter));
        });
        return returnArray;
    }
}

//Donne les indicateurs pour un (sous) lot, quelque soit le modele de départ.
function extractIndics(model, dvt, filter="", agg=false) {
    let indics = [];
    let flag = agg;

    if (model.name == filter || filter == "") {
        flag = true;
    }

    if ("children" in model) {
        let c = model.children;
        indics = c.map(x => extractIndics(x, dvt, filter, flag));
        indics = [].concat.apply([], indics);
    }

    if ("data" in model && "fiches_fdes" in model.data && flag) {
        let tac = "autoconso" in model.data ? model.data["autoconso"] : null;
        if (tac !== null) {
            tac = parseFloat(tac) / 100.0;
            let indicD = [];
            let prod = parseFloat(model.data["production"]);
            for (let q of model.data["fiches_fdes"][0].quantityIndic) {
                let nq = Object.assign({}, q);
                nq.Phase_ID = 6;
                nq.Phase_Name = "Bénéfices et charges liés à l'export d'énergie";
                nq.Quantity *= (1 - tac) * prod * dvt;
                indicD.push(nq);
            }
            var i = calcIndics(model.data["fiches_fdes"].slice(1), dvt, tac);

            for (let q of i) {
                if (q.Phase_ID == 7) {
                    let nq = Object.assign({}, q);
                    nq.Phase_ID = 6;
                    nq.Phase_Name = "Bénéfices et charges liés à l'export d'énergie";
                    nq.Quantity *= (tac - 1) / tac;
                    indicD.push(nq);
                }
            }
            i = i.concat(indicD);
        } else {
            var i = calcIndics(model.data["fiches_fdes"], dvt, tac);
        }

        for (let q of i) {
            if (q.Phase_ID == 27) q.Quantity *= -1;
        }

        for (let q of i) {
            if (q.Phase_ID == 27) {
                let nq = Object.assign({}, q);
                nq.Phase_ID = 225; //Correspond à la phase 5, à ameliorer
                nq.Phase_Name = "Bénéfices et charges au-delà du cycle de vie";
                nq.Quantity /= 3;
                i.push(nq);
            } else if (q.Phase_ID == 6) {
                let nq = Object.assign({}, q);
                nq.Phase_ID = 225; //Correspond à la phase 5, à ameliorer
                nq.Phase_Name = "Bénéfices et charges au-delà du cycle de vie";
                i.push(nq);
            }
        }

        indics = indics.concat(i);
    }

    return indics
}

function isEauEnergie(unit) {
    return unit == "kWh/an/zone" || unit == "m³/an/zone";
}


/**
 * For given FDES fiches list, get all indicateurs.
 */
function calcIndics(fiches_fdes, dvt, tac) {
    let all_indicateurs = [];
    for (var fiche of fiches_fdes) {
        if (!fiche["quantityIndic"]){
            continue;
        }
        let fiche_indicateurs = calcIDC(fiche, dvt, tac);
        if (fiche.isPEP) calcPEP(fiche_indicateurs);
        all_indicateurs = all_indicateurs.concat(fiche_indicateurs);
    }
    return all_indicateurs;
}


/**
 * Change the indicateurs list to substract the Vie en oeuvre from Total.
 */
function calcIDC(fiche, dvt, tac) {
    let fiche_indicateurs= [];
    let renew = fiche.dvt == 0 ? 1 : Math.max(1, dvt / fiche.dvt);
    for (let indicateur of fiche.quantityIndic) {
        if (!indicateur["Indicateur_Name"]){
            continue;
        }
        let newIndic = Object.assign({}, indicateur);
        if (tac !== null) newIndic.Quantity *= tac;
        newIndic.Quantity *= parseFloat(fiche.quantity);
        newIndic.Quantity *= isEauEnergie(fiche.unit) ? dvt : renew;
        fiche_indicateurs.push(newIndic);
    }
    return fiche_indicateurs;
}


/**
 * Change the indicateurs list to substract the Vie en oeuvre from Total.
 */
function calcPEP(indicateurs) {
    for (let indicateur of indicateurs) {
        if (indicateur.Phase_ID == 4 || indicateur.Phase_ID == 29) {
            let new_indicateur = Object.assign({}, indicateur);
            new_indicateur.Phase_ID = 7;
            new_indicateur.Phase_Name = "Total cycle de vie";
            new_indicateur.Quantity = -new_indicateur.Quantity;
            indicateurs.push(new_indicateur);
        }
    }
}

var getColumns = function(data) {
    var colsName = d3.nest()
        .key((d) => { return d.Indicateur_Type; })
        .key((d) => { return d.Phase_Name; })
        .entries(data);
    return colsName
}

var sumFdes = function(data) {
    var sumData = d3.nest()
        .key((d) => { return d.Indicateur_Type; })
        .key((d) => { return d.Indicateur_Name; })
        .key((d) => { return d.Phase_Name; })
        .rollup((v) => { return d3.sum(v, d => d.Quantity); })
        .entries(data);
    return sumData
}

var sumIndic = function(data) {
    var sumData = d3.nest()
        .key((d) => { return d.Indicateur_Type; })
        .key((d) => { return d.Indicateur_Name; })
        .key((d) => { return d.Phase_ID; }).sortKeys(function(a,b){
            let x = Number(a);
            let y = Number(b);
            return x < y ? -1 : x > y ? 1 : x >= y ? 0 : NaN;
        })
        .rollup(v => { return {"sum": d3.sum(v, d => d.Quantity),
                               "unit": v[0].Indicateur_Unit,
                               "indicID": v[0].Indicateur_ID}})
        .entries(data);
    return sumData
}

var sumExport = function(data) {
    var sumData = d3.nest()
        //.key((d) => { return d.Indicateur_Type; })
        .key((d) => { return d.Indicateur_Name; }).sortKeys(function(a,b){

            if(a === 'Potentiel de réchauffement climatique'){
                return -1;
            }
            if(b === 'Potentiel de réchauffement climatique'){
                return 1;
            }
            return 1;
        })
        .rollup(v => {
            return {
                    "sum": d3.sum(v, d => d.Phase_Name=="Total cycle de vie"?d.Quantity:0),
                    "unit": v[0].Indicateur_Unit,
                    "indicID": v[0].Indicateur_ID}
               }
           )
        .entries(data);
    return sumData
}