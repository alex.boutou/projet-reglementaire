import { getFDES } from "@/back/fdes.js";
import { getIndics }from "./calcIndicateur.js";

export { setModuleD };

//Do the module D calculation if possible
function setModuleD(model) {
    let newModel = JSON.parse(JSON.stringify(model));
    changeExport(newModel);
    return newModel
} 


//Change the phase of 5525 fiche
function changeExport(model) {
    if ("children" in model) model.children.map(x => changeExport(x));
    if (model.name == "Contributeur Energie") {
        for (let fdes of model.data.fiches_fdes) {
            if (fdes.fdesId == 5525) {
                for (let indic of fdes.quantityIndic) {
                    indic.Phase_ID = 6;
                    indic.Phase_Name = "Bénéfices et charges liés à l'export d'énergie";
                }
            }
        }
    }
}
