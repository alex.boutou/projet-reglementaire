export { normeName, ColonneNormeName, formatColonneNormeName, referenceName };

var changingName = {
    "Réchauffement climatique":
    "Potentiel de réchauffement climatique",

    "Appauvrissement de la couche d’ozone":
    "Potentiel de destruction de la couche d’ozone stratosphérique",

    "Acidification des sols et de l’eau":
    "Potentiel d’acidification du sol et de l’eau",

    "Eutrophisation":
    "Potentiel d’eutrophisation",

    "Formation d’ozone photochimique":
    "Potentiel de formation d’oxydants photochimiques de l’ozone troposphérique",

    "Epuisement des ressources (ADP)":
    "Potentiel de dégradation abiotique des ressources pour les éléments",

    "Epuisement des ressources abiotiques – éléments":
    "Potentiel de dégradation abiotique des ressources pour les éléments",

    "Epuisement des ressources abiotiques – combustibles fossiles":
    "Potentiel de dégradation abiotique des combustibles fossiles",

    "Utilisation de l’énergie primaire renouvelable, à l’exclusion des ressources d’énergie primaire renouvelables utilisées comme matières premières":
    "Utilisation de l’énergie primaire renouvelable à l’exclusion des ressources d’énergie employées en tant que matière première",

    "Utilisation des ressources d’énergie primaire renouvelables utilisées en tant que matières premières":
    "Utilisation de ressources énergétiques primaires renouvelables employées en tant que matière première",

    "Utilisation totale des ressources d’énergie primaire renouvelables (énergie primaire et ressources d’énergie primaire utilisées comme matières premières)":
    "Utilisation totale des ressources d’énergie primaire renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)",

    "Utilisation de l’énergie primaire non renouvelable, à l’exclusion des ressources d’énergie primaire non renouvelables utilisées comme matières premières":
    "Utilisation de l’énergie primaire non renouvelable à l’exclusion des ressources d’énergie primaire employées en tant que matière première",

    "Utilisation des ressources d’énergie primaire non renouvelables utilisées en tant que matières premières":
    "Utilisation de ressources énergétiques primaires non renouvelables employées en tant que matière première",

    "utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire utilisées comme matières premières)":
    "Utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)",

    "Utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire utilisées comme matières premières)":
    "Utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)",

    "Utilisation de matière secondaire":
    "Utilisation de matières secondaires",

    "Matériaux destinés au recyclage":
    "Matières pour le recyclage",

    "Matériaux destinés à la récupération d’énergie":
    "Matières pour la récupération d’énergie (à l’exception de l’incinération)",

    "Energie fournie à l’extérieur": "Énergie fournie à l’extérieur",

    "Changement climatique":
    "Potentiel de réchauffement climatique",

    "Acidification atmosphérique":
    "Potentiel d’acidification du sol et de l’eau",

    "Destruction de la couche d’ozone stratosphérique":
    "Potentiel de destruction de la couche d’ozone stratosphérique",

    "Déchets solides éliminés – déchets dangereux ":
    "Déchets dangereux éliminés",

    "Réchauffement climatique":
    "Potentiel de réchauffement climatique",

    "Appauvrissement de la couche d’ozone":
    "Potentiel de destruction de la couche d’ozone stratosphérique",

    "Eutrophisation":
    "Potentiel d’eutrophisation",

    "Matériaux destinés au recyclage":
    "Matières pour le recyclage",

    "Contribution à la toxicité de l’air":
    "Pollution de l’air",

    "Contribution à la toxicité de l’eau":
    "Pollution de l’eau",

    "Production de déchets dangereux":
    "Déchets dangereux éliminés",

    "Déchets éliminés dangereux":
    "Déchets dangereux éliminés",

    "Déchets éliminés non dangereux":
    "Déchets non dangereux éliminés",

    "utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire utilisées comme matières premières)":
    "Utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)",

}

var referenceName = [
    "Potentiel de réchauffement climatique",
    "Potentiel de destruction de la couche d’ozone stratosphérique",
    "Potentiel d’acidification du sol et de l’eau",
    "Potentiel d’eutrophisation",
    "Potentiel de formation d’oxydants photochimiques de l’ozone troposphérique",
    "Potentiel de dégradation abiotique des ressources pour les éléments",
    "Potentiel de dégradation abiotique des combustibles fossiles",
    "Pollution de l’air",
    "Pollution de l’eau",
    "Utilisation de l’énergie primaire renouvelable à l’exclusion des ressources d’énergie employées en tant que matière première",
    "Utilisation de ressources énergétiques primaires renouvelables employées en tant que matière première",
    "Utilisation totale des ressources d’énergie primaire renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)",
    "Utilisation de l’énergie primaire non renouvelable à l’exclusion des ressources d’énergie primaire employées en tant que matière première",
    "Utilisation de ressources énergétiques primaires non renouvelables employées en tant que matière première",
    "Utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)",
    "Utilisation totale des ressources d’énergie primaire (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)",
    "Utilisation de matières secondaires",
    "Utilisation de combustibles secondaires renouvelables",
    "Utilisation de combustibles secondaires non renouvelables",
    "Utilisation nette d’eau douce",
    "Déchets dangereux éliminés",
    "Déchets non dangereux éliminés",
    "Composants destinés à la réutilisation",
    "Matières pour le recyclage",
    "Matières pour la récupération d’énergie (à l’exception de l’incinération)",
    "Énergie fournie à l’extérieur"
]

function normeName(arrayIndic) {
    let filteredArray = filterPhase(arrayIndic);
    filteredArray = changeNames(filteredArray);
    combineNames(filteredArray);
    changeRef(filteredArray);
    return filteredArray
}

function changeNames(arrayIndic) {
    for (let indic of arrayIndic) {
        if (indic.Indicateur_Name in changingName) {
            indic.Indicateur_Name = changingName[indic.Indicateur_Name];
        }
    }

    var toFilt = ["Déchets solides éliminés – déchets radioactifs",
                  "Indicateur énergétique – Énergie procédé",
                  "Énergie procédé",
                  "Déchets valorisés total",
                  "Déchets solides valorisés total",
                  "Déchets radioactifs éliminés",
                  "Déchets éliminés radioactifs",
                  "Énergie primaire totale utilisée durant le cycle de vie"]
    return arrayIndic.filter(v => toFilt.indexOf(v.Indicateur_Name) == -1)
}

function combineNames(arrayIndic) {
    var newArrayIndic = []
    for (var indic of arrayIndic) {
        var col1 = 'Utilisation totale des ressources d’énergie primaire renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
        var col2 = 'Utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
        if (indic.Indicateur_Name == col1 || indic.Indicateur_Name == col2) {
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            newArrayIndic.push(newIndic);
        }

        var col1 = 'Indicateur énergétique – Énergie renouvelable';
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de l’énergie primaire renouvelable à l’exclusion des ressources d’énergie employées en tant que matière première';
            newIndic.Quantity *= 0.61;
            newArrayIndic.push(newIndic)
            newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de ressources énergétiques primaires renouvelables employées en tant que matière première';
            newIndic.Quantity *= 0.39;
            newArrayIndic.push(newIndic)
        }

        var col1 = 'Énergie renouvelable';
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de l’énergie primaire renouvelable à l’exclusion des ressources d’énergie employées en tant que matière première';
            newIndic.Quantity *= 0.61;
            newArrayIndic.push(newIndic)
            newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de ressources énergétiques primaires renouvelables employées en tant que matière première';
            newIndic.Quantity *= 0.39;
            newArrayIndic.push(newIndic)
        }

        var col1 = 'Indicateur énergétique – Énergie non renouvelable';
        if (indic.Indicateur_Name == col1) {
            var newIndic = JSON.parse(JSON.stringify(indic));
            indic.Indicateur_Name = 'Utilisation de l’énergie primaire non renouvelable à l’exclusion des ressources d’énergie primaire employées en tant que matière première';
            indic.Quantity *= 0.87;
            newIndic.Indicateur_Name = 'Utilisation de ressources énergétiques primaires non renouvelables employées en tant que matière première';
            newIndic.Quantity *= 0.13;
            newArrayIndic.push(newIndic)
            newIndic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            newArrayIndic.push(newIndic)
        }

        var col1 = 'Énergie non renouvelable';
        if (indic.Indicateur_Name == col1) {
            var newIndic = JSON.parse(JSON.stringify(indic));
            indic.Indicateur_Name = 'Utilisation de l’énergie primaire non renouvelable à l’exclusion des ressources d’énergie primaire employées en tant que matière première';
            indic.Quantity *= 0.87;
            newIndic.Indicateur_Name = 'Utilisation de ressources énergétiques primaires non renouvelables employées en tant que matière première';
            newIndic.Quantity *= 0.13;
            newArrayIndic.push(newIndic)
            newIndic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            newArrayIndic.push(newIndic)
        }

        var col1 = 'Consommation d’eau totale';
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Utilisation nette d’eau douce';
            indic.Quantity /= 1000.0;
            indic.Indicateur_Unit = "m³";
        }

        var col1 = 'Déchets solides éliminés – déchets non dangereux ';
        var col2 = 'Déchets solides éliminés – déchets inertes';
        if (indic.Indicateur_Name == col1 || indic.Indicateur_Name == col2) {
            indic.Indicateur_Name = 'Déchets non dangereux éliminés';
        }

        if (indic.Indicateur_Name == "Déchets éliminés inertes" ) {
            indic.Indicateur_Name = 'Déchets non dangereux éliminés';
        }

        var col1 = "Contribution à l'effet de serre";
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Potentiel de réchauffement climatique';
            indic.Quantity /= 1000.0;
            indic.Indicateur_Unit = "kg CO2 eq.";
        }

        var col1 = "contribution à la destruction de la couche d’ozone";
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Potentiel de destruction de la couche d’ozone stratosphérique';
            indic.Quantity /= 1000.0;
            indic.Indicateur_Unit = "kg CFC-11 eq.";
        }

        var col1 = "Contribution à l’eutrophisation de l’eau";
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Potentiel d’eutrophisation';
            indic.Quantity /= 1000.0;
            indic.Indicateur_Unit = "kg PO4 3- eq.";
        }

        var col1 = "Création d'ozone photochimique";
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Potentiel de formation d’oxydants photochimiques de l’ozone troposphérique';
            indic.Quantity /= 1000.0;
            indic.Indicateur_Unit = "kg C2H4 eq.";
        }

        var col1 = "Contribution à l’acidification de l’air";
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Potentiel d’acidification du sol et de l’eau';
            indic.Quantity /= 0.03178;
            indic.Indicateur_Unit = "kg SO2 eq.";
        }

        var col1 = "Consommation d'eau";
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Utilisation nette d’eau douce';
            indic.Quantity /= 1000.0;
            indic.Indicateur_Unit = "m³";
        }

        var col1 = "Indicateur énergétique – Énergie primaire totale";
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de l’énergie primaire renouvelable à l’exclusion des ressources d’énergie employées en tant que matière première';
            newIndic.Quantity *= 0.034;
            newArrayIndic.push(newIndic);
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de ressources énergétiques primaires renouvelables employées en tant que matière première';
            newIndic.Quantity *= 0.003;
            newArrayIndic.push(newIndic);
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            newIndic.Quantity *= 0.037;
            newArrayIndic.push(newIndic);
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de l’énergie primaire non renouvelable à l’exclusion des ressources d’énergie primaire employées en tant que matière première';
            newIndic.Quantity *= 0.925;
            newArrayIndic.push(newIndic);
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de ressources énergétiques primaires non renouvelables employées en tant que matière première';
            newIndic.Quantity *= 0.038;
            newArrayIndic.push(newIndic);
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            newIndic.Quantity *= 0.963;
            newArrayIndic.push(newIndic);
        }

        var col1 = "Énergie primaire totale";
        if (indic.Indicateur_Name == col1) {
            indic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de l’énergie primaire renouvelable à l’exclusion des ressources d’énergie employées en tant que matière première';
            newIndic.Quantity *= 0.034;
            newArrayIndic.push(newIndic);
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de ressources énergétiques primaires renouvelables employées en tant que matière première';
            newIndic.Quantity *= 0.003;
            newArrayIndic.push(newIndic);
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            newIndic.Quantity *= 0.037;
            newArrayIndic.push(newIndic);
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de l’énergie primaire non renouvelable à l’exclusion des ressources d’énergie primaire employées en tant que matière première';
            newIndic.Quantity *= 0.925;
            newArrayIndic.push(newIndic);
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation de ressources énergétiques primaires non renouvelables employées en tant que matière première';
            newIndic.Quantity *= 0.038;
            newArrayIndic.push(newIndic);
            var newIndic = JSON.parse(JSON.stringify(indic));
            newIndic.Indicateur_Name = 'Utilisation totale des ressources d’énergie primaire non renouvelables (énergie primaire et ressources d’énergie primaire employées en tant que matières premières)';
            newIndic.Quantity *= 0.963;
            newArrayIndic.push(newIndic);
        }
    }
    arrayIndic.push(...newArrayIndic);
}


//Take the Phase ID et change it to match RSEnv documentation
function filterPhase(arrayIndic) {
    let temp = arrayIndic.map((indic) => {
        if (indic.Phase_ID == 11) {
            indic.Phase_ID = 1;
        } else if (indic.Phase_ID == 3 || indic.Phase_ID == 28) {
            indic.Phase_ID = 2;
        } else if (indic.Phase_ID == 4 || indic.Phase_ID == 29) {
            indic.Phase_ID = 3;
        } else if (indic.Phase_ID == 5 || indic.Phase_ID == 25) {
            indic.Phase_ID = 4;
        } else if (indic.Phase_ID == 225) {
            indic.Phase_ID = 5; 
        } else if (indic.Phase_ID == 7) {
            indic.Phase_ID = 8;
        } else if (indic.Phase_ID == 27) {
            indic.Phase_ID = 7;
        }
        if ([1, 2, 3, 4, 5, 6, 7, 8].indexOf(indic.Phase_ID) >=0) return indic
    })
    return temp.filter(x => x ? true : false)
}

function changeRef(arrayIndic) {
    for (var indic of arrayIndic) {
        var idx = referenceName.indexOf(indic.Indicateur_Name);
        if (idx >= 0) {
            indic.Indicateur_ID = idx + 1;
        } else {
        }
    }
}

function ColonneNormeName(){
    return [
        {
            nom:'Production',
            id:1
        },
        {
            nom:'Construction',
            id:2
        },
        {
            nom:'Exploitation',
            id:3
        },
        {
            nom:'Fin de vie',
            id:4
        },
        {
            nom:'Bénéfices et charges au-delà du cycle de vie',
            id:5
        },
        {
            nom:"Bénéfices et charges liés à l'export d'énergie",
            id:6
        },
        {
            nom:'Bénéfices et charges liés à la valorisation des produits de construction et équipements',
            id:7
        },
        {
            nom:'Total cycle de vie',
            id:8
        }
    ]
}

function formatColonneNormeName(){

    let normeName = ColonneNormeName();
    let objectNorme = normeName.reduce(function(result, item) {
        var key = Object.keys(item);
        result[item[key[1]]] = item[key[0]];
        return result;
    }, {});
    return objectNorme;
}
