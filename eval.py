from lxml import etree
import ast
import dataset
import copy

from inies import get_fdes_attrib
from os import path

db = dataset.connect('sqlite:///BDD_INIES.db')


def id_explore(RSEE):
    """
    pour un RSEE renvoie la liste des ids des différents composants utilisés
    """
    id_list=[]
    for element in RSEE.iter("donnees_composant"):
        id_list.append(ast.literal_eval(element.find('id_fiche').text))
    return id_list

def filter_categorie(list_id):
    table=db['Fiches']
    final_list=[]
    to_keep=['Maçonnerie verticale porteur','Clôture','Plancher','Escalier','Porte extérieure','Porte de garage','Couverture de toiture','Lanterneau','Bardage','Profil en acier pour plancher','Fenêtre double vitrage']
    for id_to_look in list_id:
        cat=table.find_one(id_inies=id_to_look)['Catégorie']
        if cat in to_keep:
            final_list.append(id_to_look)
    return final_list

def id_explore_precheck(RSEE):
    """
    pour un RSEE renvoie la liste des ids des différents composants utilisés
    """
    id_list=[]
    for element in RSEE.iter("donnees_composant"):
        id_list.append(ast.literal_eval(element.find('id_fiche').text))
    for element in RSEE.iter("donnees_service"):
        id_list.append(ast.literal_eval(element.find('id_fiche').text))
    return id_list


def present_in_bdd(id_to_look,session_id):
    table = db['Fiches']
    inies=get_fdes_attrib(id_to_look,session_id)
    bdd = table.find_one(id_inies=id_to_look)
    if bdd==None or inies==None:
        return False
    else:
        return True


def id_like(id_to_look,session_id):
    """
    pour un id_inies renvoie une liste des ids des produits semblables à l'id_inies d'entrée
    """
    table = db['Fiches']
    like_list=[]
    fdes_to_look = table.find_one(id_inies=id_to_look)
    uf_to_look=fdes_to_look['Unité_fonctionnelle']
    mat_to_look=fdes_to_look['Matériau']
    if mat_to_look != None:
        like_fdes=table.find(Unité_fonctionnelle=uf_to_look,Matériau=mat_to_look)
    else:
        like_fdes=table.find(Unité_fonctionnelle=uf_to_look)
    tableind=db['Indicateurs']
    for fdes in like_fdes:
        exist=(get_fdes_attrib(fdes['id_inies'],session_id)!=None)
        if exist:
            try :
                test_exist=tableind.find_one(id_inies=fdes['id_inies'])['GWP']
                like_list.append(fdes['id_inies'])
            except:
                print('Pas d indicateurs pour la fiche '+str(fdes['id_inies']))
    return like_list

def changed_unit(id_to_look):
    """
    renvoie un booleen qui indique si l'UF de la BDD a changé par rapport à inies
    True:unité changé
    False:même unité
    """
    table = db['Indicateurs']
    return table.find_one(id_inies=id_to_look)['Autre coefficient']!=None

##########

#ARCHIVES

##########



def modif_RSEE(old_id,new_id,RSEE,session_id):
    """
    renvoie un nouveau RSEE ou old_id a été remplacé par new_id avec toutes les transformations
    nécessaires
    """
    table = db['Indicateurs']
    new_RSEE=copy.deepcopy(RSEE)
    new_fdes=get_fdes_attrib(new_id,session_id)
    if new_fdes !=None:
        if (not changed_unit(old_id) and not changed_unit(new_id)):
            print('UNITE IDENTIQUE')
            subid=new_RSEE.xpath('.//id_fiche[text()='+str(old_id)+']')
            for child in subid:
                donnees_composant=child.getparent()
                donnees_composant.find('id_fiche').text=str(new_id)
                donnees_composant.find('nom').text=str(new_fdes['Name_FDES'])
                donnees_composant.find('type_donnees').text=str(new_fdes['Declaration_Type'])
                donnees_composant.find('dve').text=str(new_fdes['DVT'])
        else:
            subid=new_RSEE.xpath('.//id_fiche[text()='+str(old_id)+']')[0]
            for child in subid:
                donnees_composant=child.getparent()
                donnees_composant.find('id_fiche').text=str(new_id)
                donnees_composant.find('nom').text=str(new_fdes['Name_FDES'])
                donnees_composant.find('type_donnees').text=str(new_fdes['Declaration_Type'])
                donnees_composant.find('dve').text=str(new_fdes['DVT'])
                old_q=ast.literal_eval(donnees_composant.find('quantite').text)
                try :
                    coef_old_id=ast.literal_eval(table.find_one(id_inies=old_id)['Autre coefficient'])
                except:
                    coef_old_id=1
                try:
                    coef_new_id=ast.literal_eval(table.find_one(id_inies=new_id)['Autre coefficient'])
                except:
                    coef_new_id=1
                donnees_composant.find('quantite').text=str(old_q*(coef_old_id/coef_new_id))
    else:
        print('FICHE SUPPRIME')
        
    #SI UF CHANGE

    return new_RSEE


