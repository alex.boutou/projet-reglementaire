from zeep import Client, helpers, exceptions
from zeep.transports import Transport
from os import path
from logging.handlers import RotatingFileHandler
from logging import getLogger, Formatter, INFO, StreamHandler
from inies import open_session, get_logger, close_session
from eval import id_explore,id_like,modif_RSEE, present_in_bdd,id_explore_precheck, filter_categorie
from lxml import etree
from eges import eges
from df_tools import build_df, final_eges, return_eges_values, modif_df
import matplotlib.pyplot as plt
import pickle
import ast
from random import choice
import dataset
import json
from pandas import DataFrame, concat,read_pickle

url = 'http://www.base-inies.fr/IniesV4/services/inieswebservice.asmx?WSDL'

#RSEE=etree.parse("data/RSEE.xml")
RSEE = etree.parse("data/RS2E_Prelude.xml")
login = 'mfouquet@combosolutions.eu'
db = dataset.connect('sqlite:///BDD_INIES.db')
password = 'yh82rp'

def precheck(RSEE,session_id):
    print('VERIFICATION FICHES')
    id_to_check=id_explore_precheck(RSEE)
    not_in_bdd=[]
    for id in id_to_check:
        if not present_in_bdd(id,session_id):
            not_in_bdd.append(id)
    if not_in_bdd==[]:
        print('PRET POUR ETUDE')
        return (True,[])
    else:
        print('ETUDE IMPOSSIBLE')
        print('FICHES A MODIFIER')
        print(not_in_bdd)
        return (False,not_in_bdd)

def trace_eges(RSEE,DVT,n_sampling):
    session_id = open_session(url)
    bool,list=precheck(RSEE,session_id)
    if bool:
        indic_collec=RSEE.xpath('.//indicateurs_performance_collection')[0]
        carb1=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='2']")[0].text)
        carb2=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='3']")[0].text)
        pce1=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='5']")[0].text)
        pce2=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='6']")[0].text)
        df=random_eval_df(RSEE,DVT,n_sampling)
        df.to_pickle('./df.rt')


        # with open('eges_list'+str(n_sampling)+'.txt',"wb") as fich:
        #     pickle.dump(eges_list, fich)          

        # with open('eges_PCE'+str(n_sampling)+'.txt',"wb") as fich:  
        #     pickle.dump(eges_PCE, fich)      

        # plt.figure()
        # plt.hist(eges_list, range = (0,2500), bins = 200)
        # plt.axvline(x=carb1,color='red')
        # plt.axvline(x=carb2,color='green')
        # plt.figure()
        # plt.hist(eges_PCE, range = (0,2500), bins = 200)
        # plt.axvline(x=pce1,color='red')
        # plt.axvline(x=pce2,color='green')
        # plt.show()

def random_eval_df(RSEE,DVT,n_sampling):
    logger = get_logger('client_inies')
    session_id = open_session(url)

    id_list=id_explore(RSEE)
    alternative_ids={}
    id_list=list(set(id_list))
    print(id_list)
    id_list=filter_categorie(id_list)
    print(id_list)
    eges_list=[]
    egesPCE_list=[]
    long=len(id_list)
    i=0
    for id in id_list:
        print((str(round(i/long*100))+'%'))
        i+=1
        similar_ids=id_like(id,session_id)
        if len(similar_ids)>1:
            alternative_ids[id]=similar_ids
    print(alternative_ids)
    with open('alternative_ids.txt', 'w') as file:
        json.dump(alternative_ids, file)

    sampling=[]
    i=0
    while i<n_sampling:
        i+=1
        dic_temp={}
        for id_to_change,new_id_list in alternative_ids.items():
            dic_temp[id_to_change]=choice(new_id_list)
        sampling.append(dic_temp)
    with open('sampling.txt', 'w') as file:
        json.dump(sampling, file)
    init_df_list=build_df(RSEE,session_id,DVT)
    root=RSEE.xpath('.//entree_projet')[0]
    sdp,tacconso,taccoge=[],[],[]
    for child in root:
        if child.tag=="batiment":
            sdp.append(ast.literal_eval(child.xpath('.//sdp')[0].text))
            tacconso.append(ast.literal_eval(child.xpath('.//autoconsommation_pv')[0].text)/100)
            taccoge.append(ast.literal_eval(child.xpath('.//autoconsommation_cogeneration')[0].text)/100)
    compteur=0
    close_session(url)
    print(return_eges_values(init_df_list,sdp))
    final_df=DataFrame()
    for sample in sampling:
        final_df_temp=DataFrame(sample,index=[i])
        final_df_temp.reindex([str(compteur+1)])
        session_id = open_session(url)
        compteur+=1
        print('SIMULATION '+str(compteur))
        df_temp=init_df_list.copy()
        for id_to_change,new_id in sample.items():
            df_temp=modif_df(df_temp,id_to_change,new_id,session_id,DVT,tacconso,taccoge)
        eges_val,egesPCE_val=return_eges_values(df_temp,sdp)
        print(eges_val)
        print(egesPCE_val)
        j=1
        for eges in eges_val:
            final_df_temp['Eges '+str(j)]=eges
            final_df_temp['EgesPCE '+str(j)]=egesPCE_val[j-1]
            j+=1
        final_df=concat([final_df,final_df_temp])
        close_session(url)
    return final_df


def trace_from_data(RSEE):
    indic_collec=RSEE.xpath('.//indicateurs_performance_collection')[0]
    carb1=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='2']")[0].text)
    carb2=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='3']")[0].text)
    pce1=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='5']")[0].text)
    pce2=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='6']")[0].text)

    with open('eges_list1000.txt',"rb") as fich:
        eges_list=pickle.load(fich)          

    with open('eges_PCE1000.txt',"rb") as fich:  
        eges_PCE=pickle.load(fich)      


    print(eges_list)
    print(eges_PCE)
    plt.figure()
    plt.hist(eges_list, range = (800,5000), bins = 200)
    plt.axvline(x=carb1,color='red')
    plt.axvline(x=carb2,color='green')

    plt.figure()
    plt.hist(eges_PCE, range = (800,5000), bins = 200)
    plt.axvline(x=pce1,color='red')
    plt.axvline(x=pce2,color='green')
    plt.show()

def find_categorie(RSEE):
    id_list=id_explore(RSEE)
    id_list=list(set(id_list))
    table=db['Fiches']
    list_cat=[]
    dic_returned={} 
    for id in id_list:
        list_cat.append(table.find_one(id_inies=id)['Catégorie'])
    for cat in list_cat:
        if cat in dic_returned:
            dic_returned[cat]+=1
        else:
            dic_returned[cat]=1
    return dic_returned

def trace_boxplot():
    with open('Résultats/Oxaya/eges_list.txt',"rb") as fich:
        eges_list=pickle.load(fich)          

    with open('Résultats/Oxaya/eges_PCE.txt',"rb") as fich:  
        eges_PCE=pickle.load(fich)      

    plt.boxplot(eges_list,0, '')
    plt.boxplot(eges_PCE,0, '')
    plt.show()

#print(find_categorie(RSEE))
#print(eges(RSEE,session_id,50))
#random_eval_df(RSEE,session_id,50,10)
trace_eges(RSEE,50,1000)
#print(id_like(8202,session_id))
#print(eges(RSEE,session_id,50))
#trace_from_data(RSEE)
#df=read_pickle('./df.rt')
#print(df)









#####################

#ANCIENNES FONCTIONS

#####################

def eval(RSEE,session_id,DVT):
    #séparer par zone avec fonction récursive
    id_list=id_explore(RSEE)
    alternative_ids={}
    id_list=list(set(id_list))
    print(id_list)
    id_list=filter_categorie(id_list)
    print(id_list)
    #i=1
    eges_list=[]
    egesPCE_list=[]
    for id in id_list:
        alternative_ids[id]=id_like(id,session_id)
    for id_to_change,new_id_list in alternative_ids.items():
        if len(new_id_list)>0:
            for new_id in new_id_list:
                print(' ')
                print('ancien id '+str(id_to_change))
                print('new id '+str(new_id))
                new_RSEE=modif_RSEE(id_to_change,new_id,RSEE,session_id)
                eges_val,egesPCE_val=(eges(new_RSEE,session_id,DVT))
                eges_list.append(eges_val)
                egesPCE_list.append(egesPCE_val)
    return eges_list,egesPCE_list

def eval_df(RSEE,session_id,DVT):
    id_list=id_explore(RSEE)
    alternative_ids={}
    id_list=list(set(id_list))
    print(id_list)
    id_list=filter_categorie(id_list)
    print(id_list)
    eges_list=[]
    egesPCE_list=[]
    long=len(id_list)
    i=0
    for id in id_list:
        print((str(round(i/long*100))+'%'))
        i+=1
        alternative_ids[id]=id_like(id,session_id)
    print(alternative_ids)
    init_df=build_df(RSEE,session_id,DVT)
    sdp=ast.literal_eval(RSEE.xpath('.//sdp')[0].text)
    print(init_df)
    print(return_eges_values(init_df,sdp))
    tacconso=ast.literal_eval(RSEE.xpath('.//autoconsommation_pv')[0].text)/100
    taccoge=ast.literal_eval(RSEE.xpath('.//autoconsommation_cogeneration')[0].text)/100
    for id_to_change,new_id_list in alternative_ids.items():
        if len(new_id_list)>0:
            for new_id in new_id_list:
                print(' ')
                print('ancien id '+str(id_to_change)+' -> new id '+str(new_id))
                eges_val,egesPCE_val=final_eges(init_df,id_to_change,new_id,session_id,DVT,sdp,tacconso,taccoge)
                eges_list.append(eges_val)
                egesPCE_list.append(egesPCE_val)

                
    return eges_list,egesPCE_list


#########

#TEST OXAYA

#########

# alternatives_ids={2957: [6339, 6339, 2957, 2959, 2958, 2914, 2964, 2965, 2966], 2959: [6339, 6339, 2957, 2959, 2958, 2914, 2964, 2965, 2966], 7442: [7442, 6314, 7446, 7902, 7922, 7940, 8415], 7446: [7442, 6314, 7446, 7902, 7922, 7940, 8415], 2966: [6339, 6339, 2957, 2959, 2958, 2914, 2964, 2965, 2966], 8216: [8216, 8465], 8482: [4489, 4490, 5063, 8482], 3497: [3494, 3495, 6331, 3496, 3497, 6331], 6315: [6315, 6839, 7556, 8488], 7995: [4787, 5792, 7995], 5697: [5697, 8533], 6339: [6339, 6339, 2957, 2959, 2958, 2914, 2964, 2965, 2966], 6743: [6742, 6743, 7979, 7830, 8157, 8352, 8353, 8354], 3835: [3835, 6332, 3829, 3830], 7659: [7652, 7654, 7659, 7661]}

# with open('notes.txt', 'r') as file:
#     sampling = json.load(file)

# with open('eges_list1000.txt',"rb") as fich:
#     eges_list=pickle.load(fich)          

# with open('eges_PCE1000.txt',"rb") as fich:  
#     eges_PCE=pickle.load(fich)     

# df=DataFrame()
# i=0
# for sample in sampling:
#     df_temp=DataFrame(sample,index=[i])
#     df_temp['Eges']=eges_list[i]
#     df_temp['EgesPCE']=eges_PCE[i]
#     df=concat([df,df_temp])
#     i+=1

# #MASKS
# new_df=df.loc[df['7446'] != 7922]
# new_df=new_df.loc[df['7446'] != 7940]

# dic={}
# for key,list in alternatives_ids.items():
#     liste_fiche=new_df[str(key)].tolist()
#     dic_returned={}
#     for fiche in list:
#         dic_returned[fiche]=0
#     for fiche in liste_fiche:
#         dic_returned[fiche]+=1
#     dic[key]=dic_returned
# print(dic)


# new_eges_list=new_df['Eges'].tolist()
# new_eges_PCE=new_df['EgesPCE'].tolist()


# indic_collec=RSEE.xpath('.//indicateurs_performance_collection')[0]
# carb1=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='2']")[0].text)
# carb2=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='3']")[0].text)
# pce1=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='5']")[0].text)
# pce2=ast.literal_eval(indic_collec.find(".//indicateurs_performance[valeur][@ref='6']")[0].text)


# plt.figure()
# plt.hist(new_eges_list, range = (0,2500), bins = 200)
# plt.axvline(x=carb1,color='red')
# plt.axvline(x=carb2,color='green')
# plt.title('Eges filtré')
# plt.figure()
# plt.hist(new_eges_PCE, range = (0,2500), bins = 200)
# plt.axvline(x=pce1,color='red')
# plt.axvline(x=pce2,color='green')
# plt.title('EgesPCE filtré')



# plt.figure()
# plt.hist(eges_list, range = (0,2500), bins = 200)
# plt.axvline(x=carb1,color='red')
# plt.axvline(x=carb2,color='green')
# plt.title('Eges')
# plt.figure()
# plt.hist(eges_PCE, range = (0,2500), bins = 200)
# plt.axvline(x=pce1,color='red')
# plt.axvline(x=pce2,color='green')
# plt.title('EgesPCE')
# plt.show()