from zeep import Client, helpers, exceptions
from zeep.transports import Transport
from os import path
from logging.handlers import RotatingFileHandler
from logging import getLogger, Formatter, INFO, StreamHandler



def get_logger(name):
    """
    Setup vizcab logger

    Parameters
    ----------
    name : str
        Name of the logger.

    Returns
    -------
    None
    """
    logger = getLogger(name)
    logger.propagate = False

    if logger.handlers == []:

        fmt = Formatter('%(asctime)s - %(message)s', "%Y-%m-%d__%H:%M:%S")

        hdl = StreamHandler()
        hdl.setLevel(INFO)
        hdl.setFormatter(fmt)

        f_log = path.expanduser('~/' + name + '.log')
        m_bytes = 128 * 1024 * 1024
        hdl2 = RotatingFileHandler(f_log, maxBytes=m_bytes, backupCount=2)
        hdl2.setLevel(INFO)
        hdl2.setFormatter(fmt)

        logger.addHandler(hdl)
        logger.addHandler(hdl2)

        logger.setLevel(INFO)

    return logger
    
url = 'http://www.base-inies.fr/IniesV4/services/inieswebservice.asmx?WSDL'
logger = get_logger('client_inies')


def open_session(url):
    """
    Login to INIES and get a Session ID.

    Parameters
    ----------
    None

    Returns
    -------
    str
        Session ID
    """
    login = 'mfouquet@combosolutions.eu'
    password = 'yh82rp'
    client = Client(url)
    try:
        session_id = client.service.Login(login, password)
    except Exception as e:
        print(e)
        print("Trying to connect anyway.")
        client.service.CloseSessionByUserLogin(login)
        session_id = client.service.Login(login, password)

    return session_id

def close_session(url):
    login = 'mfouquet@combosolutions.eu'
    client = Client(url)
    client.service.CloseSessionByUserLogin(login)


def get_fdes(id_inies, session_id,url,logger):
    """
    Get JSON file corresponding to.

    Parameters
    ----------
    id_inies : int
        FDES ID to ask.
    session_id : str
        Session ID to login

    Returns
    -------
    dict
        FDES in serializable format
    """
    client = Client(url)
    try:
        fdes = client.service.GetFDESFullDataByID(id_inies, session_id)
        return helpers.serialize_object(fdes)
    except exceptions.Fault:
        logger.exception("Failed to download FDES n°{}".format(id_inies))

def get_fdes_attrib(id_inies,session_id):
    toreturn=get_fdes(id_inies,session_id,url,logger)
    return toreturn


